# [clojure](https://hub.docker.com/_/clojure)

Multithreaded programming language https://clojure.org/

## Distributions with Clojure
* Debian
  * https://tracker.debian.org/clojure
  * https://gitlab.com/debian-packages-demo/clojure
* archlinux
  * https://wiki.archlinux.org/index.php/Clojure
  * https://www.archlinux.org/packages/community/any/clojure/
* Fedora - Centos
  * https://apps.fedoraproject.org/packages/clojure
* SdkMan
  * https://sdkman.io/sdks
* Alpine
  * https://pkgs.alpinelinux.org/packages?name=clojure
  * https://google.com/search?q=clojure+site:pkgs.alpinelinux.org
* OpenSUSE
  * https://software.opensuse.org/search?q=clojure
  * https://google.com/search?q=clojure+site:software.opensuse.org

## Official documentation
* [*Programming at the REPL: Launching a Basic REPL*
  ](https://clojure.org/guides/repl/launching_a_basic_repl)
* [*Deps and CLI Guide*](https://clojure.org/guides/deps_and_cli) for tools-deps